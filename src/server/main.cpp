#include <iostream>

#include "application.h"
#include "pythonsystem.h"

int main(int argc, char* argv[])
{
    auto app = Application::instance();

    app->addSystem(new PythonSystem());

    return app->run();
}
