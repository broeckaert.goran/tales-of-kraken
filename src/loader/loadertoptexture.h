#pragma once

#include <string>

#include "texture.h"

namespace Utility
{
class LoaderTOPTexture
{
public:
    /**
     *
     * @param path
     * @return
     */
    Structure::Texture* getTexture(const std::string& path);

private:
};
}  // namespace Utility
