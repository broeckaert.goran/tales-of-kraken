#pragma once

#include <cstdio>

namespace TalesOfPirates
{
struct Matrix4f {
public:
    /* this+0x0 */ float m[16];
};
struct Vector2f {
public:
    /* this+0x0 */ float x;
    /* this+0x4 */ float y;
};
struct Vector3f {
public:
    /* this+0x0 */ float x;
    /* this+0x4 */ float y;
    /* this+0x8 */ float z;
};

Vector3f operator+(const Vector3f& a, const Vector3f& b);

Vector3f operator/(const Vector3f& a, float b);

struct Vector4f {
public:
    /* this+0x0 */ float x;
    /* this+0x4 */ float y;
    /* this+0x8 */ float z;
    /* this+0xc */ float w;
};
struct VertexElement {
public:
    /* this+0x0 */ unsigned short Stream;
    /* this+0x2 */ unsigned short Offset;
    /* this+0x4 */ unsigned char Type;
    /* this+0x5 */ unsigned char Method;
    /* this+0x6 */ unsigned char Usage;
    /* this+0x7 */ unsigned char UsageIndex;
};
struct Quaternion {
public:
    /* this+0x0 */ float x;
    /* this+0x4 */ float y;
    /* this+0x8 */ float z;
    /* this+0xc */ float w;
};
typedef enum PrimitiveType {
    D3DPT_POlongLIST = 0x1,
    D3DPT_LINELIST = 0x2,
    D3DPT_LINESTRIP = 0x3,
    D3DPT_TRIANGLELIST = 0x4,
    D3DPT_TRIANGLESTRIP = 0x5,
    D3DPT_TRIANGLEFAN = 0x6,
    D3DPT_FORCE_DWORD = 0x7fffffff,
} PrimitiveType;

typedef enum Format {
    D3DFMT_UNKNOWN = 0x0,
    D3DFMT_R8G8B8 = 0x14,
    D3DFMT_A8R8G8B8 = 0x15,
    D3DFMT_X8R8G8B8 = 0x16,
    D3DFMT_R5G6B5 = 0x17,
    D3DFMT_X1R5G5B5 = 0x18,
    D3DFMT_A1R5G5B5 = 0x19,
    D3DFMT_A4R4G4B4 = 0x1a,
    D3DFMT_R3G3B2 = 0x1b,
    D3DFMT_A8 = 0x1c,
    D3DFMT_A8R3G3B2 = 0x1d,
    D3DFMT_X4R4G4B4 = 0x1e,
    D3DFMT_A2B10G10R10 = 0x1f,
    D3DFMT_G16R16 = 0x22,
    D3DFMT_A8P8 = 0x28,
    D3DFMT_P8 = 0x29,
    D3DFMT_L8 = 0x32,
    D3DFMT_A8L8 = 0x33,
    D3DFMT_A4L4 = 0x34,
    D3DFMT_V8U8 = 0x3c,
    D3DFMT_L6V5U5 = 0x3d,
    D3DFMT_X8L8V8U8 = 0x3e,
    D3DFMT_Q8W8V8U8 = 0x3f,
    D3DFMT_V16U16 = 0x40,
    D3DFMT_W11V11U10 = 0x41,
    D3DFMT_A2W10V10U10 = 0x43,
    D3DFMT_UYVY = 0x59565955,
    D3DFMT_YUY2 = 0x32595559,
    D3DFMT_DXT1 = 0x31545844,
    D3DFMT_DXT2 = 0x32545844,
    D3DFMT_DXT3 = 0x33545844,
    D3DFMT_DXT4 = 0x34545844,
    D3DFMT_DXT5 = 0x35545844,
    D3DFMT_D16_LOCKABLE = 0x46,
    D3DFMT_D32 = 0x47,
    D3DFMT_D15S1 = 0x49,
    D3DFMT_D24S8 = 0x4b,
    D3DFMT_D16 = 0x50,
    D3DFMT_D24X8 = 0x4d,
    D3DFMT_D24X4S4 = 0x4f,
    D3DFMT_VERTEXDATA = 0x64,
    D3DFMT_INDEX16 = 0x65,
    D3DFMT_INDEX32 = 0x66,
    D3DFMT_FORCE_DWORD = 0x7fffffff,
} Format;

typedef enum Pool {
    D3DPOOL_DEFAULT = 0x0,
    D3DPOOL_MANAGED = 0x1,
    D3DPOOL_SYSTEMMEM = 0x2,
    D3DPOOL_SCRATCH = 0x3,
    D3DPOOL_FORCE_DWORD = 0x7fffffff,
} Pool;

struct Color4c {
public:
    /* this+0x0 */ unsigned char b;
    /* this+0x1 */ unsigned char g;
    /* this+0x2 */ unsigned char r;
    /* this+0x3 */ unsigned char a;
};

struct Color4f {
public:
    /* this+0x0 */ float r;
    /* this+0x4 */ float g;
    /* this+0x8 */ float b;
    /* this+0xc */ float a;
};

struct RenderCtrlCreateInfo {
public:
    /* this+0x0 */ unsigned int idCtrl;
    /* this+0x4 */ unsigned int idDecl;
    /* this+0x8 */ unsigned int idVs;
    /* this+0xc */ unsigned int idPs;
};

class StateCtrl
{
public:
    /* this+0x0 */ unsigned char
        stateSeq[8];  // MindPower::lwObjectStateEnum ???

    void setState(unsigned int state, unsigned char value);
};

struct Material {
public:
    /* this+0x0 */ Color4f dif;
    /* this+0x10 */ Color4f amb;
    /* this+0x20 */ Color4f spe;
    /* this+0x30 */ Color4f emi;
    /* this+0x40 */ float power;
};

struct RenderStateAtom {
public:
    /* this+0x0 */ unsigned int state;  // MindPower::lwRenderStateAtomType
    /* this+0x4 */ unsigned int value0;
    /* this+0x8 */ unsigned int value1;
};

struct RenderStateValue {
public:
    /* this+0x0 */ unsigned int state;
    /* this+0x4 */ unsigned int value;
};

template <int T1, int T2>
struct RenderStateSetTemplate {
public:
    /* this+0x0 */ RenderStateValue rsv_seq[T1][T2];
};

struct TexInfo {
public:
    /* this+0x0 */ unsigned int stage;
    /* this+0x4 */ unsigned int level;
    /* this+0x8 */ unsigned int usage;
    /* this+0xc */ Format format;
    /* this+0x10 */ Pool pool;
    /* this+0x14 */ unsigned int byte_alignment_flag;
    /* this+0x18 */ unsigned int type;  // MindPower::lwTexInfoTypeEnum
    /* this+0x1c */ unsigned int width;
    /* this+0x20 */ unsigned int height;
    /* this+0x24 */ unsigned int
        colorkey_type;  // MindPower::lwColorKeyTypeEnum
    /* this+0x28 */ Color4c colorkey;
    /* this+0x2c */ char fileName[64];
    /* this+0x6c */ unsigned int
        data;  // 4bit pointer, to avoid issues with 64bit machines
    /* this+0x70 */ RenderStateAtom tssSet[8];
};

struct TexInfo_0000 {
public:
    /* this+0x0 */ unsigned int stage;
    /* this+0x4 */ unsigned int colorKeyType;  // MindPower::lwColorKeyTypeEnum
    /* this+0x8 */ Color4c colorKey;
    /* this+0xc */ Format format;
    /* this+0x10 */ char fileName[64];
    /* this+0x50 */ RenderStateSetTemplate<2, 8> tssSet;
};

struct TexInfo_0001 {
public:
    /* this+0x0 */ unsigned int stage;
    /* this+0x4 */ unsigned int level;
    /* this+0x8 */ unsigned int usage;
    /* this+0xc */ Format format;
    /* this+0x10 */ Pool pool;
    /* this+0x14 */ unsigned int byte_alignment_flag;
    /* this+0x18 */ unsigned int type;  // MindPower::lwTexInfoTypeEnum
    /* this+0x1c */ unsigned int width;
    /* this+0x20 */ unsigned int height;
    /* this+0x24 */ unsigned int
        colorKey_type;  // MindPower::lwColorKeyTypeEnum
    /* this+0x28 */ Color4c colorKey;
    /* this+0x2c */ char fileName[64];
    /* this+0x6c */ unsigned int
        data;  // 4bit pointer, to avoid issues with 64bit machines
    /* this+0x70 */ RenderStateSetTemplate<2, 8> tssSet;
};

struct MaterialTexInfo {
public:
    /* this+0x0 */ float opacity;
    /* this+0x4 */ unsigned int
        transparency_type;  // MindPower::lwMtlTexInfoTransparencyTypeEnum
    /* this+0x8 */ Material mtl;
    /* this+0x4c */ RenderStateAtom rs_set[8];
    /* this+0xac */ TexInfo tex_seq[4];
};

struct BlendInfo {
public:
    union {
    public:
        /* this+0x0 */ unsigned char index[4];
        /* this+0x0 */ unsigned int indexd;
    };
    /* this+0x4 */ float weight[4];
};

struct SubsetInfo {
public:
    /* this+0x0 */ unsigned int primitive_num;
    /* this+0x4 */ unsigned int start_index;
    /* this+0x8 */ unsigned int vertex_num;
    /* this+0xc */ unsigned int min_index;
};

struct MeshInfo {
public:
    /* this+0x0 */ struct MeshInfoHeader {
    public:
        /* this+0x0 */ unsigned int fvf;
        /* this+0x4 */ PrimitiveType pt_type;
        /* this+0x8 */ unsigned int vertex_num;
        /* this+0xc */ unsigned int index_num;
        /* this+0x10 */ unsigned int subset_num;
        /* this+0x14 */ unsigned int bone_index_num;
        /* this+0x18 */ unsigned int bone_infl_factor;
        /* this+0x1c */ unsigned int vertex_element_num;
        /* this+0x20 */ RenderStateAtom rs_set[8];
    } header;
    /* this+0x80 */ Vector3f* vertex_seq;
    /* this+0x84 */ Vector3f* normal_seq;
    /* this+0x88 */ Vector2f* texcoord_seq[4];
    /* this+0x88 */ Vector2f* texcoord0_seq;
    /* this+0x8c */ Vector2f* texcoord1_seq;
    /* this+0x90 */ Vector2f* texcoord2_seq;
    /* this+0x94 */ Vector2f* texcoord3_seq;
    /* this+0x98 */ unsigned int* vercol_seq;
    /* this+0x9c */ unsigned int* index_seq;
    /* this+0xa0 */ unsigned int* bone_index_seq;
    /* this+0xa4 */ BlendInfo* blend_seq;
    /* this+0xa8 */ SubsetInfo* subset_seq;
    /* this+0xac */ VertexElement* vertex_element_seq;
};

struct MeshInfo_0000 {
public:
    /* this+0x0 */ struct MeshInfoHeader {
    public:
        /* this+0x0 */ unsigned int fvf;
        /* this+0x4 */ PrimitiveType pt_type;
        /* this+0x8 */ unsigned int vertex_num;
        /* this+0xc */ unsigned int index_num;
        /* this+0x10 */ unsigned int subset_num;
        /* this+0x14 */ unsigned int bone_index_num;
        /* this+0x18 */ RenderStateSetTemplate<2, 8> rs_set;
    } header;
    /* this+0x98 */ Vector3f* vertex_seq;
    /* this+0x9c */ Vector3f* normal_seq;
    /* this+0xa0 */ Vector2f* texcoord_seq[4];
    /* this+0xa0 */ Vector2f* texcoord0_seq;
    /* this+0xa4 */ Vector2f* texcoord1_seq;
    /* this+0xa8 */ Vector2f* texcoord2_seq;
    /* this+0xac */ Vector2f* texcoord3_seq;
    /* this+0xb0 */ unsigned int* vercol_seq;
    /* this+0xb4 */ unsigned int* index_seq;
    /* this+0xb8 */ BlendInfo* blend_seq;
    /* this+0xbc */ SubsetInfo subset_seq[16];
    /* this+0x1bc */ unsigned char bone_index_seq[25];
};

struct MeshInfo_0003 {
public:
    /* this+0x0 */ struct MeshInfoHeader {
    public:
        /* this+0x0 */ unsigned int fvf;
        /* this+0x4 */ PrimitiveType pt_type;
        /* this+0x8 */ unsigned int vertex_num;
        /* this+0xc */ unsigned int index_num;
        /* this+0x10 */ unsigned int subset_num;
        /* this+0x14 */ unsigned int bone_index_num;
        /* this+0x18 */ RenderStateAtom rs_set[8];
    } header;
    /* this+0x78 */ Vector3f* vertex_seq;
    /* this+0x7c */ Vector3f* normal_seq;
    /* this+0x80 */ Vector2f* texcoord_seq[4];
    /* this+0x80 */ Vector2f* texcoord0_seq;
    /* this+0x84 */ Vector2f* texcoord1_seq;
    /* this+0x88 */ Vector2f* texcoord2_seq;
    /* this+0x8c */ Vector2f* texcoord3_seq;
    /* this+0x90 */ unsigned int* vercol_seq;
    /* this+0x94 */ unsigned int* index_seq;
    /* this+0x98 */ BlendInfo* blend_seq;
    /* this+0x9c */ SubsetInfo subset_seq[16];
    /* this+0x19c */ unsigned char bone_index_seq[25];
};

struct HelperDummyInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ Matrix4f mat;
    /* this+0x44 */ Matrix4f mat_local;
    /* this+0x84 */ unsigned int parent_type;
    /* this+0x88 */ unsigned int parent_id;
};

struct HelperDummyInfo_1000 {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ Matrix4f mat;
};

class Box
{
public:
    /* this+0x0 */ Vector3f c;
    /* this+0xc */ Vector3f r;
};

class Box_1001
{
public:
    /* this+0x0 */ Vector3f p;
    /* this+0xc */ Vector3f s;
};

struct HelperBoxInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ unsigned int type;
    /* this+0x8 */ unsigned int state;
    /* this+0xc */ Box box;
    /* this+0x24 */ Matrix4f mat;
    /* this+0x64 */ char name[32];
};

class Plane
{
public:
    /* this+0x0 */ float a;
    /* this+0x4 */ float b;
    /* this+0x8 */ float c;
    /* this+0xc */ float d;
};

struct HelperMeshFaceInfo {
public:
    /* this+0x0 */ unsigned int vertex[3];
    /* this+0xc */ unsigned int adj_face[3];
    /* this+0x18 */ Plane plane;
    /* this+0x28 */ Vector3f center;
};

struct HelperMeshInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ unsigned int type;
    /* this+0x8 */ char name[32];
    /* this+0x28 */ unsigned int state;
    /* this+0x2c */ unsigned int sub_type;
    /* this+0x30 */ Matrix4f mat;
    /* this+0x70 */ Box box;
    /* this+0x88 */ Vector3f* vertex_seq;
    /* this+0x8c */ HelperMeshFaceInfo* face_seq;
    /* this+0x90 */ unsigned int vertex_num;
    /* this+0x94 */ unsigned int face_num;
};

struct BoundingBoxInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ Box box;
    /* this+0x1c */ Matrix4f mat;
};

class Sphere
{
public:
    /* this+0x0 */ Vector3f c;
    /* this+0xc */ float r;
};

struct BoundingSphereInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ Sphere sphere;
    /* this+0x14 */ Matrix4f mat;
};

struct HelperInfo {
public:
    /* this+0x4 */ unsigned int type;  // MindPower::lwHelperInfoType
    /* this+0x8 */ HelperDummyInfo* dummy_seq;
    /* this+0xc */ HelperBoxInfo* box_seq;
    /* this+0x10 */ HelperMeshInfo* mesh_seq;
    /* this+0x14 */ BoundingBoxInfo* bbox_seq;
    /* this+0x18 */ BoundingSphereInfo* bsphere_seq;
    /* this+0x1c */ unsigned int dummy_num;
    /* this+0x20 */ unsigned int box_num;
    /* this+0x24 */ unsigned int mesh_num;
    /* this+0x28 */ unsigned int bbox_num;
    /* this+0x2c */ unsigned int bsphere_num;

    int _LoadHelperDummyInfo(FILE* fp, unsigned int version);

    int _LoadHelperBoxInfo(FILE* fp, unsigned int version);

    int _LoadHelperMeshInfo(FILE* fp, unsigned int version);

    int _LoadBoundingBoxInfo(FILE* fp, unsigned int version);

    int _LoadBoundingSphereInfo(FILE* fp, unsigned int version);

    int Load(FILE* fp, unsigned int version);
};

struct BoneBaseInfo {
public:
    /* this+0x0 */ char name[64];
    /* this+0x40 */ unsigned int id;
    /* this+0x44 */ unsigned int parent_id;
};

struct BoneDummyInfo {
public:
    /* this+0x0 */ unsigned int id;
    /* this+0x4 */ unsigned int parent_bone_id;
    /* this+0x8 */ Matrix4f mat;
};

class Matrix33f
{
public:
    /* this+0x0 */ float m[3][3];
};

class Matrix43f
{
public:
    /* this+0x0 */ float m[4][3];
};

struct BoneKeyInfo {
public:
    /* this+0x0 */ Matrix43f* mat43_seq;
    /* this+0x4 */ Matrix4f* mat44_seq;
    /* this+0x8 */ Vector3f* pos_seq;
    /* this+0xc */ Quaternion* quat_seq;
};

class AnimationDataBone
{
public:
    /* this+0x4 */ struct BoneInfoHeader {
    public:
        /* this+0x0 */ unsigned int bone_num;
        /* this+0x4 */ unsigned int frame_num;
        /* this+0x8 */ unsigned int dummy_num;
        /* this+0xc */ unsigned int key_type;  // MindPower::lwBoneKeyInfoType
    } _header;
    /* this+0x14 */ BoneBaseInfo* _base_seq;
    /* this+0x18 */ BoneDummyInfo* _dummy_seq;
    /* this+0x1c */ BoneKeyInfo* _key_seq;
    /* this+0x20 */ Matrix4f* _invmat_seq;

    int Load(FILE* fp, unsigned int version);

    virtual int Load(const char* file);
};

class AnimDataMatrix
{
public:
    /* this+0x4 */ Matrix43f* _mat_seq;
    /* this+0x8 */ unsigned int _frame_num;

    int Load(FILE* fp, unsigned int version);
};

struct KeyFloat {
public:
    /* this+0x0 */ unsigned int key;
    /* this+0x4 */ unsigned int slerp_type;
    /* this+0x8 */ float data;
};

class IAnimKeySetFloat
{
public:
    virtual unsigned int SetKeySequence(KeyFloat* seq, unsigned int num) = 0;
};

class AnimKeySetFloat : public IAnimKeySetFloat
{
public:
    /* this+0x4 */ KeyFloat* _data_seq;
    /* this+0x8 */ unsigned int _data_num;
    /* this+0xc */ unsigned int _data_capacity;

    unsigned int SetKeySequence(KeyFloat* seq, unsigned int num);
};

struct AnimDataMtlOpacity {
public:
    /* this+0x4 */ IAnimKeySetFloat* _aks_ctrl;

    int Load(FILE* fp, unsigned int version);
};

struct AnimDataTexUV {
public:
    /* this+0x4 */ Matrix4f* _mat_seq;
    /* this+0x8 */ unsigned int _frame_num;

    int Load(FILE* fp, unsigned int version);
};

class AnimDataTexImg
{
public:
    /* this+0x4 */ TexInfo* _data_seq;
    /* this+0x8 */ unsigned int _data_num;
    /* this+0xc */ char _tex_path[260];

    int Load(FILE* fp, unsigned int version);
};

class AnimDataInfo
{
public:
    /* this+0x4 */ AnimationDataBone* anim_bone;
    /* this+0x8 */ AnimDataMatrix* anim_mat;
    /* this+0xc */ AnimDataMtlOpacity* anim_mtlopac[16];
    /* this+0x4c */ AnimDataTexUV* anim_tex[16][4];
    /* this+0x14c */ AnimDataTexImg* anim_img[16][4];

    int Load(FILE* fp, unsigned int version);
};

int MtlTexInfo_Load(MaterialTexInfo* info, FILE* fp, unsigned int version);

int LoadMtlTexInfo(MaterialTexInfo** out_buf, unsigned int* out_num, FILE* fp,
                   unsigned int version);

int MeshInfo_Load(MeshInfo* info, FILE* fp, unsigned int version);

struct GeomObjInfo {
public:
    /* this+0x4 */ struct GeomObjInfoHeader {
    public:
        /* this+0x0 */ unsigned int id;
        /* this+0x4 */ unsigned int parent_id;
        /* this+0x8 */ unsigned int
            type;  // MindPower::lwModelObjectLoadType ???
        /* this+0xc */ Matrix4f mat_local;
        /* this+0x4c */ RenderCtrlCreateInfo rcci;
        /* this+0x5c */ StateCtrl state_ctrl;
        /* this+0x64 */ unsigned int mtl_size;
        /* this+0x68 */ unsigned int mesh_size;
        /* this+0x6c */ unsigned int helper_size;
        /* this+0x70 */ unsigned int anim_size;
    } header;
    /* this+0x78 */ unsigned int mtl_num;
    /* this+0x7c */ MaterialTexInfo* mtl_seq;
    /* this+0x80 */ MeshInfo mesh;
    /* this+0x130 */ HelperInfo helper_data;
    /* this+0x160 */ AnimDataInfo anim_data;

    int Load(FILE* fp, unsigned int version);

    virtual int Load(const char* file);
};

struct ModelObjInfo {
public:
    struct ModelObjInfoHeader {
    public:
        /* this+0x0 */ unsigned int type;
        /* this+0x4 */ unsigned int addr;
        /* this+0x8 */ unsigned int size;
    };

    /* this+0x4 */ unsigned int geom_obj_num;
    /* this+0x8 */ GeomObjInfo* geom_obj_seq[32];
    /* this+0x88 */ HelperInfo helper_data;

    virtual int Load(const char* file);
};

struct ModelHeadInfo {
public:
    /* this+0x0 */ unsigned int mask;
    /* this+0x4 */ unsigned int version;
    /* this+0x8 */ char decriptor[64];
};

class ITreeNode
{
public:
    // int __thiscall EnumTree(unsigned int __cdecl ,ITreeNode*,void*);
    virtual int EnumTree(unsigned int(class ITreeNode*, void*), void*,
                         unsigned int) = 0;

    virtual int InsertChild(ITreeNode*, ITreeNode*) = 0;
};

struct __find_info {
    /*off:0000;siz:0004*/ public:
    ITreeNode* node;
    /*off:0004;siz:0004*/ public:
    unsigned int handle;
};

unsigned int __tree_proc_find_node(ITreeNode*, void*);

class TreeNode : public ITreeNode
{
public:
    /* this+0x4 */ void* _data;
    /* this+0x8 */ ITreeNode* _parent;
    /* this+0xc */ ITreeNode* _child;
    /* this+0x10 */ ITreeNode* _sibling;

    void _SetRegisterID(void*);

    int EnumTree(unsigned int(class ITreeNode*, void*), void*, unsigned int);

    int InsertChild(ITreeNode*, ITreeNode*);
};

struct /*siz:88*/ ModelNodeHeadInfo {
public:
    /*off:0000;siz:0004*/ public:
    unsigned int handle;
    /*off:0004;siz:0004*/ public:
    unsigned int type;
    /*off:0008;siz:0004*/ public:
    unsigned int id;
    /*off:0012;siz:0064*/ public:
    char descriptor[64];
    /*off:0076;siz:0004*/ public:
    unsigned int parent_handle;
    /*off:0080;siz:0004*/ public:
    unsigned int link_parent_id;
    /*off:0084;siz:0004*/ public:
    unsigned int link_id;
};

struct /*siz:100*/ ModelNodeInfo {
public:
    /*off:0004;siz:0088*/ public:
    ModelNodeHeadInfo _head;
    /*off:0092;siz:0004*/ public:
    void* _data;
    /*off:0096;siz:0004*/ public:
    void* _param;

    int Load(FILE*, unsigned int);
};

class ModelInfo
{
public:
    /* this+0x0 */ ModelHeadInfo _head;
    /* this+0x48 */ ITreeNode* _obj_tree;

    int Load(const char* file);
};
}  // namespace TalesOfPirates
