#include "data.h"

#include <cstring>
#include <iostream>

namespace TalesOfPirates
{
Vector3f operator+(const Vector3f& a, const Vector3f& b)
{
    Vector3f r = {a.x + b.x, a.y + b.y, a.z + b.z};
    return r;
}

Vector3f operator/(const Vector3f& a, float b)
{
    Vector3f r = {a.x / b, a.y / b, a.z / b};
    return r;
}

void StateCtrl::setState(unsigned int state, unsigned char value)
{
    this->stateSeq[state] = value;
}

int HelperInfo::_LoadHelperDummyInfo(FILE* fp, unsigned int version)
{
    if (version >= 0x1001) {
        fread(&(this->dummy_num), sizeof(unsigned int), 1, fp);
        this->dummy_seq = new HelperDummyInfo[this->dummy_num];
        fread(this->dummy_seq, sizeof(HelperDummyInfo), this->dummy_num, fp);
    } else if (version <= 0x1000) {
        fread(&(this->dummy_num), sizeof(unsigned int), 1, fp);
        HelperDummyInfo_1000* old_s = new HelperDummyInfo_1000[this->dummy_num];
        fread(old_s, sizeof(HelperDummyInfo_1000), this->dummy_num, fp);
        this->dummy_seq = new HelperDummyInfo[this->dummy_num];
        for (unsigned int i = 0; i < this->dummy_num; i++) {
            this->dummy_seq[i].id = old_s[i].id;
            this->dummy_seq[i].mat = old_s[i].mat;
            this->dummy_seq[i].parent_type = 0;
            this->dummy_seq[i].parent_id = 0;
        };
        delete[] old_s;
    };
    return 0;
}

int HelperInfo::_LoadHelperBoxInfo(FILE* fp, unsigned int version)
{
    fread(&(this->box_num), sizeof(unsigned int), 1, fp);
    this->box_seq = new HelperBoxInfo[this->box_num];
    fread(this->box_seq, sizeof(HelperBoxInfo), this->box_num, fp);
    if (version <= 0x1001) {
        Box_1001 old_b = Box_1001();
        for (unsigned int i = 0; i < this->box_num; i++) {
            Box* b = &this->box_seq[i].box;
            old_b.p = b->c;
            old_b.s = b->r;
            b->r = old_b.s / 2;
            b->c = old_b.p + b->r;
        };
    };
    return 0;
}

int HelperInfo::_LoadHelperMeshInfo(FILE* fp, unsigned int version)
{
    fread(&(this->mesh_num), sizeof(unsigned int), 1, fp);
    this->mesh_seq = new HelperMeshInfo[this->mesh_num];
    for (unsigned int i = 0; i < this->mesh_num; i++) {
        HelperMeshInfo* info = &this->mesh_seq[i];
        fread(&(info->id), sizeof(unsigned int), 1, fp);
        fread(&(info->type), sizeof(unsigned int), 1, fp);
        fread(&(info->sub_type), sizeof(unsigned int), 1, fp);
        fread(&(info->name), 20, 1, fp);  // sizeof(info->name) = 20;
        fread(&(info->state), sizeof(unsigned int), 1, fp);
        fread(&(info->mat), sizeof(Matrix4f), 1, fp);
        fread(&(info->box), sizeof(Box), 1, fp);
        fread(&(info->vertex_num), sizeof(unsigned int), 1, fp);
        fread(&(info->face_num), sizeof(unsigned int), 1, fp);
        info->vertex_seq = new Vector3f[info->vertex_num];
        info->face_seq = new HelperMeshFaceInfo[info->face_num];
        fread(info->vertex_seq, sizeof(Vector3f), info->vertex_num, fp);
        fread(info->face_seq, sizeof(HelperMeshFaceInfo), info->face_num, fp);
    };
    if (version <= 0x1001) {
        Box_1001 old_b = Box_1001();
        for (unsigned int i = 0; i < this->mesh_num; i++) {
            Box* b = &this->mesh_seq[i].box;
            old_b.p = b->c;
            old_b.s = b->r;
            b->r = old_b.s / 2;
            b->c = old_b.p + b->r;
        };
    };
    return 0;
}

int HelperInfo::_LoadBoundingBoxInfo(FILE* fp, unsigned int version)
{
    fread(&(this->bbox_num), sizeof(unsigned int), 1, fp);
    this->bbox_seq = new BoundingBoxInfo[this->bbox_num];
    fread(this->bbox_seq, sizeof(BoundingBoxInfo), this->bbox_num, fp);
    if (version <= 0x1001) {
        Box_1001 old_b = Box_1001();
        for (unsigned int i = 0; i < this->bbox_num; i++) {
            Box* b = &this->bbox_seq[i].box;
            old_b.p = b->c;
            old_b.s = b->r;
            b->r = old_b.s / 2;
            b->c = old_b.p + b->r;
        };
    };
    return 0;
}

int HelperInfo::_LoadBoundingSphereInfo(FILE* fp, unsigned int version)
{
    fread(&(this->bsphere_num), sizeof(unsigned int), 1, fp);
    this->bsphere_seq = new BoundingSphereInfo[this->bsphere_num];
    fread(this->bsphere_seq, sizeof(BoundingSphereInfo), this->bsphere_num, fp);
    return 0;
}

int HelperInfo::Load(FILE* fp, unsigned int version)
{
    if (version == 0) {
        unsigned int old_version;
        fread(&(old_version), sizeof(unsigned int), 1,
              fp);  // No messing around with 'version' O_o
    };
    fread(&(this->type), sizeof(unsigned int), 1, fp);
    if (this->type & 0x1) this->_LoadHelperDummyInfo(fp, version);
    if (this->type & 0x2) this->_LoadHelperBoxInfo(fp, version);
    if (this->type & 0x4) this->_LoadHelperMeshInfo(fp, version);
    if (this->type & 0x10) this->_LoadBoundingBoxInfo(fp, version);
    if (this->type & 0x20) this->_LoadBoundingSphereInfo(fp, version);
    return 0;
}

int AnimationDataBone::Load(FILE* fp, unsigned int version)
{
    if (version == 0) {
        unsigned int old_version;
        fread(&old_version, sizeof(unsigned int), 1, fp);
        int x = 0;
    };
    if (this->_base_seq) {
        return -1;
    };
    fread(&(this->_header), sizeof(AnimationDataBone::BoneInfoHeader), 1, fp);
    this->_base_seq = new BoneBaseInfo[this->_header.bone_num];
    this->_key_seq = new BoneKeyInfo[this->_header.bone_num];
    this->_invmat_seq = new Matrix4f[this->_header.bone_num];
    this->_dummy_seq = new BoneDummyInfo[this->_header.dummy_num];
    fread(this->_base_seq, sizeof(BoneBaseInfo), this->_header.bone_num, fp);
    fread(this->_invmat_seq, sizeof(Matrix4f), this->_header.bone_num, fp);
    fread(this->_dummy_seq, sizeof(BoneDummyInfo), this->_header.dummy_num, fp);
    if (this->_header.key_type == 1) {
        for (unsigned int i = 0; i < this->_header.bone_num; i++) {
            BoneKeyInfo* key = &this->_key_seq[i];
            key->mat43_seq = new Matrix43f[this->_header.frame_num];
            fread(key->mat43_seq, sizeof(Matrix43f), this->_header.frame_num,
                  fp);
        };
    } else if (this->_header.key_type == 2) {
        for (unsigned int i = 0; i < this->_header.bone_num; i++) {
            BoneKeyInfo* key = &this->_key_seq[i];
            key->mat44_seq = new Matrix4f[this->_header.frame_num];
            fread(key->mat44_seq, sizeof(Matrix4f), this->_header.frame_num,
                  fp);
        };
    } else if (this->_header.key_type == 3) {
        if (version >= 0x1003) {
            for (unsigned int i = 0; i < this->_header.bone_num; i++) {
                BoneKeyInfo* key = &this->_key_seq[i];
                key->pos_seq = new Vector3f[this->_header.frame_num];
                fread(key->pos_seq, sizeof(Vector3f), this->_header.frame_num,
                      fp);
                key->quat_seq = new Quaternion[this->_header.frame_num];
                fread(key->quat_seq, sizeof(Quaternion),
                      this->_header.frame_num, fp);
            };
        } else {
            for (unsigned int i = 0; i < this->_header.bone_num; i++) {
                BoneKeyInfo* key = &this->_key_seq[i];
                unsigned int pos_num = this->_base_seq[i].parent_id == -1
                                           ? this->_header.frame_num
                                           : 1;
                key->pos_seq = new Vector3f[this->_header.frame_num];
                fread(key->pos_seq, sizeof(Vector3f), pos_num, fp);
                if (pos_num == 1) {
                    for (unsigned int j = 1; j < this->_header.frame_num; j++) {
                        key->pos_seq[i] = key->pos_seq[0];
                    };
                };
                key->quat_seq = new Quaternion[this->_header.frame_num];
                fread(key->quat_seq, sizeof(Quaternion),
                      this->_header.frame_num, fp);
            };
        };
    };
    return 0;
}

int AnimationDataBone::Load(const char* file)
{
    int ret = -1;
    FILE* fp = fopen(file, "rb");
    if (fp) {
        unsigned int version;
        fread(&version, sizeof(unsigned int), 1, fp);
        if (version < 0x1000) {
            version = 0;
            char buf[256];
            std::printf(buf, "old animation file: %s, need re-export it", file);
        };
        if (this->Load(fp, version) >= 0) {  // Overloaded version
            ret = 0;
        }
    };
    if (fp) fclose(fp);
    return ret;
}

int AnimDataMatrix::Load(FILE* fp, unsigned int version)
{
    fread(&(this->_frame_num), sizeof(unsigned int), 1, fp);
    this->_mat_seq = new Matrix43f[this->_frame_num];
    fread(this->_mat_seq, sizeof(Matrix43f), this->_frame_num, fp);
    return 0;
}

unsigned int AnimKeySetFloat::SetKeySequence(KeyFloat* seq, unsigned int num)
{
    _data_seq = seq;
    _data_num = num;
    _data_capacity = num;
    return 0;
}

int AnimDataMtlOpacity::Load(FILE* fp, unsigned int version)
{
    int ret = -1;
    unsigned int num;
    fread(&(num), sizeof(unsigned int), 1, fp);
    KeyFloat* seq = new KeyFloat[num];
    fread(seq, sizeof(KeyFloat), num, fp);
    this->_aks_ctrl = new AnimKeySetFloat();
    if (this->_aks_ctrl->SetKeySequence(seq, num) >= 0) {
        ret = 0;
    };
    return ret;
}

int AnimDataTexUV::Load(FILE* fp, unsigned int version)
{
    fread(&(this->_frame_num), sizeof(unsigned int), 1, fp);
    this->_mat_seq = new Matrix4f[this->_frame_num];
    fread(this->_mat_seq, sizeof(Matrix4f), this->_frame_num, fp);
    return 0;
}

int AnimDataTexImg::Load(FILE* fp, unsigned int version)
{
    int ret = 0;
    if (version == 0) {
        char buf[256];
        sprintf(buf, "old version file, need to re-export it");
        ret = -1;
    } else {
        fread(&(this->_data_num), sizeof(unsigned int), 1, fp);
        this->_data_seq = new TexInfo[this->_data_num];
        fread(this->_data_seq, sizeof(TexInfo), this->_data_num, fp);
    };
    return ret;
}

int AnimDataInfo::Load(FILE* fp, unsigned int version)
{
    if (version == 0) {
        unsigned int old_version;
        fread(&(old_version), sizeof(unsigned int), 1, fp);
    };
    unsigned int data_bone_size;
    unsigned int data_mat_size;
    unsigned int data_mtlopac_size[16];
    unsigned int data_texuv_size[16][4];
    unsigned int data_teximg_size[16][4];
    fread(&(data_bone_size), sizeof(unsigned int), 1, fp);
    fread(&(data_mat_size), sizeof(unsigned int), 1, fp);
    if (version >= 4001) {
        fread(&(data_mtlopac_size[0]), sizeof(data_mtlopac_size), 1, fp);
    };
    fread(&(data_texuv_size[0][0]), sizeof(data_texuv_size), 1, fp);
    fread(&(data_teximg_size[0][0]), sizeof(data_teximg_size), 1, fp);
    if (version > 0) {
        this->anim_bone = new AnimationDataBone();
        this->anim_bone->Load(fp, version);
    };
    if (data_mat_size > 0) {
        this->anim_mat = new AnimDataMatrix();
        this->anim_mat->Load(fp, version);
    };
    if (version >= 0x1005) {
        for (unsigned int i = 0; i < 16; i++) {
            if (data_mtlopac_size[i] == 0) continue;
            this->anim_mtlopac[i] = new AnimDataMtlOpacity();
            this->anim_mtlopac[i]->Load(fp, version);
        };
    };
    for (unsigned int i = 0; i < 16; i++) {
        for (unsigned int j = 0; j < 4; j++) {
            if (data_texuv_size[i][j] == 0) continue;
            this->anim_tex[i][j] = new AnimDataTexUV();
            this->anim_tex[i][j]->Load(fp, version);
        };
    };
    for (unsigned int i = 0; i < 16; i++) {
        for (unsigned int j = 0; j < 4; j++) {
            if (data_teximg_size[i][j] == 0) continue;
            this->anim_img[i][j] = new AnimDataTexImg();
            this->anim_img[i][j]->Load(fp, version);
        };
    };
    return 0;
}

int MtlTexInfo_Load(struct MaterialTexInfo* info, FILE* fp,
                    unsigned int version)
{
    if (version >= 0x1000) {
        fread(&(info->opacity), sizeof(float), 1, fp);
        fread(&(info->transparency_type), sizeof(unsigned int), 1, fp);
        fread(&(info->mtl), sizeof(Material), 1, fp);
        fread(&(info->rs_set), sizeof(RenderStateAtom) * 8, 1, fp);
        fread(&(info->tex_seq), sizeof(TexInfo) * 4, 1, fp);
    } else if (version == 2) {
        fread(&(info->opacity), sizeof(float), 1, fp);
        fread(&(info->transparency_type), sizeof(unsigned int), 1, fp);
        fread(&(info->mtl), sizeof(Material), 1, fp);
        fread(&(info->rs_set), sizeof(RenderStateAtom) * 8, 1, fp);
        fread(&(info->tex_seq), sizeof(TexInfo) * 4, 1, fp);
    } else if (version == 1) {
        fread(&(info->opacity), sizeof(float), 1, fp);
        fread(&(info->transparency_type), sizeof(unsigned int), 1, fp);
        fread(&(info->mtl), sizeof(Material), 1, fp);
        RenderStateSetTemplate<2, 8> rsm;
        fread(&rsm, sizeof(RenderStateSetTemplate<2, 8>), 1, fp);
        TexInfo_0001 tex_info[4];
        fread(&tex_info, sizeof(TexInfo_0001) * 4, 1, fp);
        for (unsigned int i = 0; i < 8; i++) {
            RenderStateValue* rsv = &(rsm.rsv_seq[0][i]);
            if (rsv->state == -1) break;
            unsigned int v;
            if (rsv->state == 25) {
                v = 5;
            } else if (rsv->state == 24) {
                v = 129;
            } else {
                v = rsv->value;
            };
            info->rs_set[i].state = rsv->state;
            info->rs_set[i].value0 = v;
            info->rs_set[i].value1 = v;
        };
        for (unsigned int i = 0; i < 4; i++) {
            TexInfo_0001* p = &(tex_info[i]);
            if (p->stage == -1) break;
            TexInfo* t = &(info->tex_seq[i]);
            t->level = p->level;
            t->usage = p->usage;
            t->pool = p->pool;
            t->type = p->type;
            t->width = p->width;
            t->height = p->height;
            t->stage = p->stage;
            t->format = p->format;
            t->colorkey = p->colorKey;
            t->colorkey_type = p->colorKey_type;
            t->byte_alignment_flag = p->byte_alignment_flag;
            strcpy(t->fileName, p->fileName);
            for (unsigned int j = 0; j < 8; j++) {
                RenderStateValue* rsv = &(p->tssSet.rsv_seq[j][0]);
                if (rsv->state == -1) break;
                t->tssSet[j].state = rsv->state;
                t->tssSet[j].value0 = rsv->value;
                t->tssSet[j].value1 = rsv->value;
            };
        };
    } else if (version == 0) {
        fread(&(info->mtl), sizeof(Material), 1, fp);
        RenderStateSetTemplate<2, 8> rsm;
        fread(&(rsm), sizeof(RenderStateSetTemplate<2, 8>), 1, fp);
        TexInfo_0000 tex_info[4];
        fread(&(tex_info), sizeof(TexInfo_0000) * 4, 1, fp);
        for (unsigned int i = 0; i < 8; i++) {
            RenderStateValue* rsv = &(rsm.rsv_seq[0][i]);
            if (rsv->state == -1) break;
            unsigned int v;
            if (rsv->state == 25) {
                v = 5;
            } else if (rsv->state == 24) {
                v = 129;
            } else {
                v = rsv->value;
            };
            info->rs_set[i].state = rsv->state;
            info->rs_set[i].value0 = v;
            info->rs_set[i].value1 = v;
        };
        for (unsigned int i = 0; i < 4; i++) {
            TexInfo_0000* p = &(tex_info[i]);
            if (p->stage == -1) break;
            TexInfo* t = &(info->tex_seq[i]);
            t->level = 1;
            t->usage = 0;
            t->pool = (Pool)0;
            t->type = 0;
            t->stage = p->stage;
            t->format = p->format;
            t->colorkey = p->colorKey;
            t->colorkey_type = p->colorKeyType;
            t->byte_alignment_flag = 0;
            strcpy(t->fileName, p->fileName);
            for (unsigned int j = 0; j < 8; j++) {
                RenderStateValue* rsv = &(p->tssSet.rsv_seq[0][j]);
                if (rsv->state == -1) break;
                t->tssSet[j].state = rsv->state;
                t->tssSet[j].value0 = rsv->value;
                t->tssSet[j].value1 = rsv->value;
            };
        };
        if (info->tex_seq[0].format == 26) info->tex_seq[0].format = (Format)25;
    } else {
        return -1;
    };
    info->tex_seq[0].pool = (Pool)1;
    info->tex_seq[0].level = 3;
    int transp_flag = 0;
    unsigned int i;
    for (i = 0; i < 8; i++) {
        RenderStateAtom* rsa = &info->rs_set[i];
        if (rsa->state == -1) break;
        if ((rsa->state == 20) && ((rsa->value0 == 2) || (rsa->value0 == 4)))
            transp_flag = 1;
        if ((rsa->state == 137) && (rsa->value0 == 0)) transp_flag++;
    };
    if ((transp_flag == 1) && (i < 7))
        ;
    // RSA_VALUE(&(info->rs_set[i]), 137, 0);  //endcrypt
    return 0;
}

int LoadMtlTexInfo(struct MaterialTexInfo** out_buf, unsigned int* out_num,
                   FILE* fp, unsigned int version)
{
    MaterialTexInfo* buf;
    unsigned int num = 0;
    if (version == 0) {
        unsigned int old_version;
        fread(&old_version, sizeof(unsigned int), 1, fp);
        version = old_version;
    };
    fread(&num, sizeof(unsigned int), 1, fp);
    buf = new MaterialTexInfo[num];
    for (unsigned int i = 0; i < num; i++) {
        MtlTexInfo_Load(&(buf[i]), fp, version);
    }
    *out_buf = buf;
    *out_num = num;
    return 0;
}

int MeshInfo_Load(struct MeshInfo* info, FILE* fp, unsigned int version)
{
    if (version == 0) {
        unsigned int old_version;
        fread(&(old_version), sizeof(unsigned int), 1, fp);
        version = old_version;
    };
    // Loading in the MeshInfoHeader:
    if (version >= 0x1004) {
        fread(&(info->header), sizeof(MeshInfo::MeshInfoHeader), 1, fp);
    } else if (version >= 0x1003) {
        MeshInfo_0003::MeshInfoHeader header;
        fread(&(header), sizeof(MeshInfo_0003::MeshInfoHeader), 1, fp);
        info->header.fvf = header.fvf;
        info->header.pt_type = header.pt_type;
        info->header.vertex_num = header.vertex_num;
        info->header.index_num = header.index_num;
        info->header.subset_num = header.subset_num;
        info->header.bone_index_num = header.bone_index_num;
        info->header.bone_infl_factor = info->header.bone_index_num > 0 ? 2 : 0;
        info->header.vertex_element_num = 0;
    } else if ((version >= 0x1000) || (version == 1)) {
        MeshInfo_0003::MeshInfoHeader header;
        fread(&header, sizeof(MeshInfo_0003::MeshInfoHeader), 1, fp);
        info->header.fvf = header.fvf;
        info->header.pt_type = header.pt_type;
        info->header.vertex_num = header.vertex_num;
        info->header.index_num = header.index_num;
        info->header.subset_num = header.subset_num;
        info->header.bone_index_num = header.bone_index_num;
        info->header.bone_infl_factor = info->header.bone_index_num > 0 ? 2 : 0;
        info->header.vertex_element_num = 0;
    } else if (version == 0) {
        MeshInfo_0000::MeshInfoHeader header;
        fread(&header, sizeof(MeshInfo_0000::MeshInfoHeader), 1, fp);
        info->header.fvf = header.fvf;
        info->header.pt_type = header.pt_type;
        info->header.vertex_num = header.vertex_num;
        info->header.index_num = header.index_num;
        info->header.subset_num = header.subset_num;
        info->header.bone_index_num = header.bone_index_num;
        for (unsigned int j = 0; j < 8; j++) {
            RenderStateValue* rsv = &header.rs_set.rsv_seq[j][0];
            if (rsv->state == -1) break;
            unsigned int v;
            if (rsv->state == 147) {
                v = 2;
            } else {
                v = rsv->value;
            };
            info->header.rs_set[j].state = rsv->state;
            info->header.rs_set[j].value0 = v;
            info->header.rs_set[j].value1 = v;
        };
    } else {
        std::cout << "Invalid version" << std::endl;
    };
    // Loading in the MeshInfo:
    if (version >= 0x1004) {
        if (info->header.vertex_element_num > 0) {
            info->vertex_element_seq =
                new VertexElement[info->header.vertex_element_num];
            fread(info->vertex_element_seq, sizeof(VertexElement),
                  info->header.vertex_element_num, fp);
        };
        if (info->header.vertex_num > 0) {
            info->vertex_seq = new Vector3f[info->header.vertex_num];
            fread(info->vertex_seq, sizeof(Vector3f), info->header.vertex_num,
                  fp);
        };
        if (info->header.fvf & 0x10) {
            info->normal_seq = new Vector3f[info->header.vertex_num];
            fread(info->normal_seq, sizeof(Vector3f), info->header.vertex_num,
                  fp);
        };
        if (info->header.fvf & 0x100) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x200) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x300) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            info->texcoord2_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord2_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x400) {
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            info->texcoord2_seq = new Vector2f[info->header.vertex_num];
            info->texcoord3_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord2_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord3_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        };
        if (info->header.fvf & 0x40) {
            info->vercol_seq = new unsigned int[info->header.vertex_num];
            fread(info->vercol_seq, sizeof(unsigned int),
                  info->header.vertex_num, fp);
        };
        if (info->header.bone_index_num > 0) {
            info->blend_seq = new BlendInfo[info->header.vertex_num];
            info->bone_index_seq =
                new unsigned int[info->header.bone_index_num];
            fread(info->blend_seq, sizeof(BlendInfo), info->header.vertex_num,
                  fp);
            fread(info->bone_index_seq, sizeof(unsigned int),
                  info->header.bone_index_num, fp);
        };
        if (info->header.index_num > 0) {
            info->index_seq = new unsigned int[info->header.index_num];
            fread(info->index_seq, sizeof(unsigned int), info->header.index_num,
                  fp);
        };
        if (info->header.subset_num > 0) {
            info->subset_seq = new SubsetInfo[info->header.subset_num];
            fread(info->subset_seq, sizeof(SubsetInfo), info->header.subset_num,
                  fp);
        };
    } else {
        info->subset_seq = new SubsetInfo[info->header.subset_num];
        fread(info->subset_seq, sizeof(SubsetInfo), info->header.subset_num,
              fp);
        info->vertex_seq = new Vector3f[info->header.vertex_num];
        fread(info->vertex_seq, sizeof(Vector3f), info->header.vertex_num, fp);
        if (info->header.fvf & 0x10) {
            info->normal_seq = new Vector3f[info->header.vertex_num];
            fread(info->normal_seq, sizeof(Vector3f), info->header.vertex_num,
                  fp);
        };
        if (info->header.fvf & 0x100) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x200) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x300) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            info->texcoord2_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord2_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        } else if (info->header.fvf & 0x400) {
            info->texcoord0_seq = new Vector2f[info->header.vertex_num];
            info->texcoord1_seq = new Vector2f[info->header.vertex_num];
            info->texcoord2_seq = new Vector2f[info->header.vertex_num];
            info->texcoord3_seq = new Vector2f[info->header.vertex_num];
            fread(info->texcoord0_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord1_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord2_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
            fread(info->texcoord3_seq, sizeof(Vector2f),
                  info->header.vertex_num, fp);
        };
        if (info->header.fvf & 0x40) {
            info->vercol_seq = new unsigned int[info->header.vertex_num];
            fread(info->vercol_seq, sizeof(unsigned int),
                  info->header.vertex_num, fp);
        };
        if (info->header.fvf & 0x1000) {
            info->blend_seq = new BlendInfo[info->header.vertex_num];
            unsigned char* byte_index_seq =
                new unsigned char[info->header.bone_index_num];
            fread(info->blend_seq, sizeof(BlendInfo), info->header.vertex_num,
                  fp);
            fread(byte_index_seq, sizeof(char), info->header.bone_index_num,
                  fp);
            info->bone_index_seq =
                new unsigned int[info->header.bone_index_num];
            for (unsigned int i = 0; i < info->header.bone_index_num; i++) {
                info->bone_index_seq[i] = (char)byte_index_seq[i];
            };
            delete[] byte_index_seq;
        };
        if (info->header.index_num > 0) {
            info->index_seq = new unsigned int[info->header.index_num];
            fread(info->index_seq, sizeof(unsigned int), info->header.index_num,
                  fp);
        };
    };
    return 0;
}

int GeomObjInfo::Load(FILE* fp, unsigned int version)
{
    fread(
        &(this->header), sizeof(GeomObjInfo::GeomObjInfoHeader), 1,
        fp);  // read a structure of MindPower::GeomObjInfo::lwGeomObjInfoHeader
    this->header.state_ctrl.setState(5, 0);
    this->header.state_ctrl.setState(3, 1);
    if (this->header.mtl_size > 100000) {
        return -1;
    };
    if (this->header.mtl_size > 0)
        LoadMtlTexInfo(&this->mtl_seq, &this->mtl_num, fp, version);
    if (this->header.mesh_size > 0) MeshInfo_Load(&(this->mesh), fp, version);
    if (this->header.helper_size > 0) this->helper_data.Load(fp, version);
    if (this->header.anim_size > 0) this->anim_data.Load(fp, version);
    return 0;
}

int GeomObjInfo::Load(const char* file)
{
    FILE* fp = fopen(file, "rb");
    if (!fp) return -1;
    unsigned int version;
    fread(&version, 4, 1, fp);
    int ret = this->Load(fp, version);
    if (fp) fclose(fp);
    return ret;
}

int ModelObjInfo::Load(const char* file)
{
    FILE* fp = fopen(file, "rb");
    if (!fp) return -1;
    unsigned int version;
    fread(&version, sizeof(unsigned int), 1, fp);
    unsigned int obj_num;
    fread(&obj_num, sizeof(unsigned int), 1, fp);
    ModelObjInfo::ModelObjInfoHeader header[33];
    fread(&header, sizeof(ModelObjInfo::ModelObjInfoHeader), obj_num, fp);
    this->geom_obj_num = 0;
    for (unsigned int i = 0; i < obj_num; i++) {
        fseek(fp, header[i].addr, 0);
        if (header[i].type == 1) {
            this->geom_obj_seq[this->geom_obj_num] = new GeomObjInfo();
            if (version == 0) {
                unsigned int old_version;
                fread(&old_version, 4, 1, fp);
            }
            this->geom_obj_seq[this->geom_obj_num]->Load(fp, version);
            this->geom_obj_num++;
        } else if (header[i].type == 2) {
            this->helper_data.Load(fp, version);
        };
    };
    if (fp) fclose(fp);
    return 0;
}

unsigned int __tree_proc_find_node(class ITreeNode*, void*) { return 0; }

void TreeNode::_SetRegisterID(void*) {}

int TreeNode::EnumTree(unsigned int(class ITreeNode*, void*), void*,
                       unsigned int)
{
    return 0;
}

int TreeNode::InsertChild(ITreeNode*, ITreeNode*) { return 0; }

int ModelNodeInfo::Load(FILE*, unsigned int) { return 0; }

int ModelInfo::Load(const char* file)
{
    int ret = -1;
    FILE* fp = fopen(file, "rb");
    if (!fp) {
        fclose(fp);
        return ret;
    };
    fread(&this->_head, sizeof(ModelHeadInfo), 1, fp);
    unsigned int obj_num;
    fread(&obj_num, sizeof(unsigned int), 1, fp);
    if (obj_num == 0) {
        ret = 0;
        if (fp) fclose(fp);
        return ret;
    };
    ITreeNode* tree_node = 0;
    ModelNodeInfo* node_info = 0;
    for (unsigned int i = 0; i < obj_num; i++) {
        node_info = new ModelNodeInfo();
        if (node_info->Load(fp, this->_head.version) < 0) {
            delete node_info;
            if (fp) fclose(fp);
            return ret;
        }
        tree_node = new TreeNode();
        ((TreeNode*)tree_node)->_SetRegisterID((void*)node_info);  // MindPower::TreeNode::_SetRegisterID(unsigned
                                                                   // int)
        if (this->_obj_tree == 0) {
            this->_obj_tree = tree_node;
        } else {
            __find_info param;
            param.handle = node_info->_head.parent_handle;
            param.node = 0;
            this->_obj_tree->EnumTree(__tree_proc_find_node, &param, 0);
            if (param.node == 0) {
                if (fp) fclose(fp);
                return ret;
            };
            if (param.node->InsertChild(0, tree_node) < 0) {
                if (fp) fclose(fp);
                return ret;
            };
        };
    };
    ret = 0;
    if (fp) fclose(fp);
    return ret;
}
};  // namespace TalesOfPirates