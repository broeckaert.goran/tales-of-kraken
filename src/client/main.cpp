#include <atomic>
#include <coroutine>
#include <future>
#include <iostream>
#include <thread>

#include "logger.h"
#include "pythonsystem.h"
#include "rendersystem.h"

template <typename R>
class Event
{
public:
    auto operator co_await() const noexcept
    {
        struct awaiter {
            explicit awaiter(const Event<R>& event) : mEvent(event) {}

            bool await_ready() const noexcept { return false; }
            void await_suspend(std::coroutine_handle<> handle) noexcept
            {
                const_cast<Event<R>&>(mEvent).mHandle = handle;
            }
            const R& await_resume() { return mEvent.mValue; }

        private:
            const Event<R>& mEvent;
        };

        return awaiter{*this};
    }

    void set(R value)
    {
        mValue = value;
        mHandle.resume();
    }

private:
    std::coroutine_handle<> mHandle;
    R mValue;
};

template <>
class Event<void>
{
public:
    auto operator co_await() const noexcept
    {
        struct awaiter {
            explicit awaiter(const Event<void>& event) : mEvent(event) {}

            bool await_ready() const noexcept { return false; }
            void await_suspend(std::coroutine_handle<> handle) noexcept
            {
                const_cast<Event<void>&>(mEvent).mHandle = handle;
            }
            void await_resume() {}

        private:
            const Event<void>& mEvent;
        };

        return awaiter{*this};
    }

    void set() { mHandle.resume(); }

private:
    std::coroutine_handle<> mHandle;
};

namespace detail
{
    struct final_awaitable
    {
        bool await_ready() const noexcept { return false; }

        template <typename Promise>
        std::coroutine_handle<> await_suspend(std::coroutine_handle<Promise> handle) noexcept
        {
            return handle.promise().mContinuation;
        }

        void await_resume() noexcept {}
    };
}

template <typename R>
class Task
{
public:
    struct promise_type;
    using HandleType = std::coroutine_handle<promise_type>;

    struct promise_type {
        auto get_return_object()
        {
            return Task<R>(HandleType::from_promise(*this));
        }
        std::suspend_always initial_suspend() noexcept { return {}; }
        detail::final_awaitable final_suspend() noexcept { return {}; }

        void return_value(R v) { mValue = v; }
        void unhandled_exception() {}

        void continuation(std::coroutine_handle<> handle)
        {
            mContinuation = handle;
        }

    private:
        friend struct detail::final_awaitable;
        friend class Task<R>;

        R mValue;

        std::coroutine_handle<> mContinuation;
    };

    explicit Task(HandleType handle) : mHandle(handle) {}
    R& value() const { return mHandle.promise().mValue; }

    auto operator co_await() const noexcept
    {
        struct awaiter {
            explicit awaiter(HandleType handle) : mHandle(handle) {}

            bool await_ready() const noexcept { return !mHandle || mHandle.done(); }
            std::coroutine_handle<> await_suspend(std::coroutine_handle<> handle) noexcept
            {
                mHandle.promise().continuation(handle);
                return mHandle;
            }
            const R& await_resume()
            {
                return mHandle.promise().mValue;
            }

        private:
            HandleType mHandle;
        };

        return awaiter{mHandle};
    }

private:
    HandleType mHandle;
};

template <>
class Task<void>
{
public:
    struct promise_type;
    using HandleType = std::coroutine_handle<promise_type>;

    struct promise_type {
        auto get_return_object()
        {
            return Task<void>(HandleType::from_promise(*this));
        }
        std::suspend_never initial_suspend() noexcept { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }

        void return_void() {}
        void unhandled_exception() {}

    private:
        friend class Task<void>;
    };

    explicit Task(HandleType handle) : mHandle(handle) {}

    void value() const { return; }

private:
    HandleType mHandle;
};

Task<std::string> event_waiting(Event<void>& event)
{
    co_await event;

    co_return std::string("This is a test string.");
}

Task<void> event_void(Event<void>& event)
{
    LOGT("This is working perfectly.");

    Task<std::string> task = event_waiting(event);

    std::string result = co_await task;

    LOGT(result);

    co_return;
}

int main(int argc, char* argv[])
{
    // auto app = Application::instance();

    Logger::instance()->setLogLevel(Logger::TRACE);

    // app->addSystem(new PythonSystem());
    // app->addSystem(new RenderSystem());

    // return app->run();

    Event<void> event;
    event_void(event);

    LOGT("Before the void event has been set.");
    event.set();
    LOGT("After the void event has been set.");

    return 0;
}
