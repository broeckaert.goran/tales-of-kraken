include(FindPackageHandleStandardArgs)

if (WIN32)
    find_path(
        FREEIMAGE_INCLUDE_DIR
        NAMES FreeImage.h
        PATHS
            ${FreeImage_LIBDIR}
            $ENV{PROGRAMFILES}/include
        DOC "The directory which contains FreeImage.h")
    find_library(
        FREEIMAGE_LIBRARY
        NAMES FreeImage
        PATHS
            ${FreeImage_LIBDIR}
            $ENV{PROGRAMFILES}/lib
        DOC "The FreeImage Libraries")
else()
    find_path(
        FREEIMAGE_INCLUDE_DIR
        NAMES FreeImage.h
        PATHS
            /usr/include
            /usr/local/include
            /opt/local/include
        DOC "The directory which contains FreeImage.h")
    find_library(
        FREEIMAGE_LIBRARY
	NAMES freeimage
        PATHS
            /usr/lib64
            /usr/lib
            /usr/local/lib64
            /usr/llocal/lib
            /opt/local/include
        DOC "The FreeImage Libraries")
endif()

find_package_handle_standard_args(FreeImage DEFAULT_MSG FREEIMAGE_INCLUDE_DIR FREEIMAGE_LIBRARY)
if (FREEIMAGE_FOUND)
    set(FREEIMAGE_LIBRARIES ${FREEIMAGE_LIBRARY})
    set(FREEIMAGE_INCLUDE_DIRS ${FREEIMAGE_INCLUDE_DIR})
endif()

mark_as_advanced(FREEIMAGE_INCLUDE_DIR FREEIMAGE_LIBRARY)
