from application import Application
from logger import log_info, log_error, log_trace

def build():
    log_info("Initializing all the variables.")

def update(delta : float):
    log_error("Hello World from inside Python!")
    log_trace("This is a test, that shouldn't be printed.")

    application = Application()
    entities = application.get_entities()
